//
//  DKLoginViewController.m
//  DaKa
//
//  Created by 刁俊方 on 2020/1/5.
//  Copyright © 2020 刁俊方. All rights reserved.
//

#import "DKLoginViewController.h"

#define kLeftPadding 20
#define kGetCodeBtnWidth 122

@interface DKLoginViewController ()<UITextFieldDelegate>
{
    UIImageView *imageView;
    
    UIImageView *iconImgView;
    UILabel *titleLab;
    
    UITextField *jobNumberTF;
    UILabel *passwordPathLineLab;
    UITextField *passwordTF;
    UILabel *passwordPathLineLab2;
    
    UITextField *phoneNumberTF;
    UILabel *lineLab;
    UITextField *noteCodeTF;
    UILabel *verticalLineLab;
    UIButton *getCodeBtn;
    UILabel *lineLab2;
    UIButton *loginBtn;
    UIColor *registTitleColor;
    NSTimer *timer;
    int _count;
    CFAbsoluteTime oldTime;
    
    float _offset;
    UIButton *seeButton;
    NSDictionary *loginInfoDic;//登录信息
    NSString *phoneNumber;
    
    UIButton *changeLoginPathBtn;
    
    BOOL first_login;
    
    NSString *showLoginName;
    
    NSDictionary *loginPathDic;
    
    BOOL oldVersion;//北京银行是否是老版本
    NSTimeInterval lastTapTimeStamp;
    
    NewBJLoginPath loginPath;
}

@end

@implementation DKLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor whiteColor];
    _offset=0;
    loginPath = 0;
    [self creatSubViews];
    [self reDrawRect];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enterBackGround:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
    [self updateloginBtn];
    [self updateGetCodeBtn];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    if (timer) {
        [timer invalidate];
        timer = nil;
    }
}

-(void)enterBackGround:(NSNotification*)sender {
    oldTime = CFAbsoluteTimeGetCurrent();
    if (timer) {
        [timer invalidate];
        timer = nil;
    }
}
-(void)enterForeground:(NSNotification *)sender {
}
-(void)creatSubViews{
    iconImgView=[[UIImageView alloc] init];
    iconImgView.image=[UIImage imageNamed:@"login_logo_image"];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(uploadLogTappedWithGesture:)];
    [iconImgView addGestureRecognizer:tapGesture];
    iconImgView.userInteractionEnabled = YES;
    [self.view addSubview:iconImgView];
    titleLab=[[UILabel alloc] init];
    titleLab.font=[UIFont boldSystemFontOfSize:30.0f];
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *appName = [infoDictionary objectForKey:@"CFBundleDisplayName"];
    titleLab.text=appName;
    titleLab.textColor=[UIColor dk_colorWithHexString:mainThemeColor];
    //    [self.view addSubview:titleLab];
    
    jobNumberTF = [[UITextField alloc]init];
    jobNumberTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    jobNumberTF.delegate = self;
    jobNumberTF.font = [UIFont systemFontOfSize:16.0f];
    jobNumberTF.placeholder=GetLocalResStr(@"dk_phone_number_placeholder");
    jobNumberTF.tintColor = [UIColor dk_colorWithHexString: mainBackgroundGrayColor];
    [jobNumberTF setValue:[UIFont systemFontOfSize:16.0f] forKeyPath:@"_placeholderLabel.font"];
    [jobNumberTF setValue:[UIColor dk_colorWithHexString:mainTextGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    [[jobNumberTF valueForKey:@"_clearButton"] setImage:[UIImage imageNamed:@"js_clearButton_phone"] forState:UIControlStateNormal];
    jobNumberTF.textColor = [UIColor dk_colorWithHexString: mainBlackColor];
    jobNumberTF.keyboardType = UIKeyboardTypeDefault;//键盘显示类型
    jobNumberTF.returnKeyType = UIReturnKeyNext;
    [jobNumberTF addTarget:self action:@selector(changeField:) forControlEvents:UIControlEventEditingDidEndOnExit];
    [jobNumberTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.view addSubview:jobNumberTF];
    
    passwordPathLineLab=[[UILabel alloc]init];
    passwordPathLineLab.backgroundColor=[UIColor colorWithRed:226/255.0 green:226/255.0 blue:226/255.0 alpha:1];
    [self.view addSubview:passwordPathLineLab];
    
    passwordTF = [[UITextField alloc]init];
#ifdef DK_ENABLE_SAFETY_KEYBOARD
    passwordTF = [[MXCustomTextFiled alloc]init];
#endif
    passwordTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    passwordTF.placeholder=GetLocalResStr(@"dk_input_password");
    passwordTF.delegate = self;
    passwordTF.font = [UIFont systemFontOfSize:16.0f];
    passwordTF.tintColor = [UIColor dk_colorWithHexString: mainBackgroundGrayColor];
    [passwordTF setValue:[UIFont systemFontOfSize:16.0f] forKeyPath:@"_placeholderLabel.font"];
    [passwordTF setValue:[UIColor dk_colorWithHexString:mainTextGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    [[passwordTF valueForKey:@"_clearButton"] setImage:[UIImage imageNamed:@"js_clearButton_phone"] forState:UIControlStateNormal];
    passwordTF.textColor = [UIColor dk_colorWithHexString: mainBlackColor];
    passwordTF.keyboardType = UIKeyboardTypeDefault;//键盘显示类型
    passwordTF.returnKeyType = UIReturnKeyDone;
    passwordTF.secureTextEntry=YES;
    [passwordTF addTarget:self action:@selector(changeField:) forControlEvents:UIControlEventEditingDidEndOnExit];
    [passwordTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    passwordPathLineLab2=[[UILabel alloc]init];
    passwordPathLineLab2.backgroundColor=[UIColor colorWithRed:226/255.0 green:226/255.0 blue:226/255.0 alpha:1];
    [self.view addSubview:passwordPathLineLab2];

    [self.view addSubview:passwordTF];
    
    seeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    seeButton.backgroundColor = [UIColor clearColor];
    seeButton.selected = YES;
    [seeButton setImage:[UIImage imageNamed:@"login_no_see_password_phone"] forState:UIControlStateSelected];
    [seeButton setImage:[UIImage imageNamed:@"login_see_password_phone"] forState:UIControlStateNormal];
    [seeButton addTarget:self action:@selector(change:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:seeButton];
    
    phoneNumberTF = [[UITextField alloc]init];
    phoneNumberTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    phoneNumberTF.delegate = self;
    phoneNumberTF.font = [UIFont systemFontOfSize:16.0f];
    phoneNumberTF.placeholder=GetLocalResStr(@"dk_phone_number_placeholder");
    phoneNumberTF.tintColor = [UIColor dk_colorWithHexString: mainBackgroundGrayColor];
    [phoneNumberTF setValue:[UIFont systemFontOfSize:16.0f] forKeyPath:@"_placeholderLabel.font"];
    [phoneNumberTF setValue:[UIColor dk_colorWithHexString:mainTextGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    [[phoneNumberTF valueForKey:@"_clearButton"] setImage:[UIImage imageNamed:@"js_clearButton_phone"] forState:UIControlStateNormal];
    phoneNumberTF.textColor = [UIColor dk_colorWithHexString: mainBlackColor];
    phoneNumberTF.keyboardType = UIKeyboardTypePhonePad;//键盘显示类型
    phoneNumberTF.returnKeyType = UIReturnKeyNext;
    [phoneNumberTF addTarget:self action:@selector(changeField:) forControlEvents:UIControlEventEditingDidEndOnExit];
    [phoneNumberTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.view addSubview:phoneNumberTF];
    
    lineLab=[[UILabel alloc]init];
    lineLab.backgroundColor=[UIColor colorWithRed:226/255.0 green:226/255.0 blue:226/255.0 alpha:1];
    [self.view addSubview:lineLab];
    
    noteCodeTF = [[UITextField alloc]init];
    noteCodeTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    noteCodeTF.placeholder=GetLocalResStr(@"dk_noteCode_placeholder");
    noteCodeTF.delegate = self;
    noteCodeTF.font = [UIFont systemFontOfSize:16.0f];
    noteCodeTF.tintColor = [UIColor dk_colorWithHexString: mainBackgroundGrayColor];
    [noteCodeTF setValue:[UIFont systemFontOfSize:16.0f] forKeyPath:@"_placeholderLabel.font"];
    [noteCodeTF setValue:[UIColor dk_colorWithHexString:mainTextGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    [[noteCodeTF valueForKey:@"_clearButton"] setImage:[UIImage imageNamed:@"js_clearButton_phone"] forState:UIControlStateNormal];
    noteCodeTF.textColor = [UIColor dk_colorWithHexString: mainBlackColor];
    noteCodeTF.keyboardType = UIKeyboardTypePhonePad;//键盘显示类型
    noteCodeTF.returnKeyType = UIReturnKeyDone;
    [noteCodeTF addTarget:self action:@selector(changeField:) forControlEvents:UIControlEventEditingDidEndOnExit];
    [noteCodeTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.view addSubview:noteCodeTF];
    
    verticalLineLab=[[UILabel alloc]init];
    verticalLineLab.backgroundColor=[UIColor colorWithRed:189/255.0 green:189/255.0 blue:189/255.0 alpha:1];
    [self.view addSubview:verticalLineLab];
    
    getCodeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [getCodeBtn setTitle:GetLocalResStr(@"dk_send_noteCode") forState:UIControlStateNormal];
    [getCodeBtn setTitleColor:[UIColor dk_colorWithHexString:mainTextGrayColor] forState:UIControlStateNormal];
    getCodeBtn.backgroundColor = [UIColor clearColor];
    getCodeBtn.titleLabel.font = [UIFont systemFontOfSize:16.0f];
    getCodeBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    //    getCodeBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
    [getCodeBtn addTarget:self action:@selector(getNoteCodeAuthCode) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:getCodeBtn];
    
    lineLab2=[[UILabel alloc]init];
    lineLab2.backgroundColor=[UIColor colorWithRed:226/255.0 green:226/255.0 blue:226/255.0 alpha:1];
    [self.view addSubview:lineLab2];
    
    loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [loginBtn setTitle:GetLocalResStr(@"dk_login") forState:UIControlStateNormal];
    [loginBtn setTitleColor:[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:0.4] forState:UIControlStateNormal];
    loginBtn.titleLabel.font = [UIFont systemFontOfSize:17.0f];
    loginBtn.layer.cornerRadius = 22;
    [loginBtn addTarget:self action:@selector(login) forControlEvents:UIControlEventTouchUpInside];
    [self setLoginBtnBackgroundColorWithEnable:NO];
    [self.view addSubview:loginBtn];
    
    changeLoginPathBtn= [UIButton buttonWithType:UIButtonTypeCustom];
    if (loginPath==NewBJAuthCodeLoginPath) {
        [changeLoginPathBtn setTitle:GetLocalResStr(@"dk_use_password_login_hint") forState:UIControlStateNormal];
    }else{
        [changeLoginPathBtn setTitle:GetLocalResStr(@"dk_use_code_login_hint") forState:UIControlStateNormal];
    }
    [changeLoginPathBtn setTitleColor:[UIColor dk_colorWithHexString:mainThemeColor] forState:UIControlStateNormal];
    changeLoginPathBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    changeLoginPathBtn.titleLabel.font = [UIFont systemFontOfSize:16.0f];
    [changeLoginPathBtn addTarget:self action:@selector(changeLoginPath:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:changeLoginPathBtn];
    
    if (showLoginName.length>0) {
        if (loginPath==NewBJPasswordLoginPath) {
            jobNumberTF.text=showLoginName;
        }else{
            phoneNumberTF.text=showLoginName;
        }
    }
    
    [self updateLoginInterface];
    
}

- (void)setLoginBtnBackgroundColorWithEnable: (BOOL)enable {
    loginBtn.backgroundColor = enable ? [UIColor dk_colorWithHexString:mainThemeColor] : [UIColor dk_colorWithHexString:mainBackgroundGrayColor];
}

-(void)reDrawRect{
    UIImage *icon=[UIImage imageNamed:@"login_logo_image"];
    CGFloat iconWidth=icon.size.width;
    CGFloat iconHeight=icon.size.height;
    CGFloat iconImgViewLeftPadding=(appWidth-iconWidth)/2;
    if (isIphoneX) {
        iconImgView.frame=CGRectMake(iconImgViewLeftPadding, 109.5, iconWidth, iconHeight);
    }else{
        iconImgView.frame=CGRectMake(iconImgViewLeftPadding, 85, iconWidth, iconHeight);
    }
    
    jobNumberTF.frame=CGRectMake(kLeftPadding, CGRectGetMaxY(iconImgView.frame)+60, appWidth-kLeftPadding*2, 52.5);
    passwordPathLineLab.frame=CGRectMake(CGRectGetMinX(jobNumberTF.frame), CGRectGetMaxY(jobNumberTF.frame)+1, appWidth-kLeftPadding*2, 1);
    passwordTF.frame=CGRectMake(CGRectGetMinX(jobNumberTF.frame), CGRectGetMaxY(passwordPathLineLab.frame), appWidth-kLeftPadding*2-22-kLeftPadding-3.5, 52.5);
    seeButton.frame=CGRectMake(appWidth-22-kLeftPadding-3.5, CGRectGetMaxY(passwordPathLineLab.frame)+15.25, 22, 22);
    passwordPathLineLab2.frame=CGRectMake(CGRectGetMinX(jobNumberTF.frame), CGRectGetMaxY(passwordTF.frame), appWidth-kLeftPadding*2, 1);
    
    phoneNumberTF.frame=CGRectMake(kLeftPadding, CGRectGetMaxY(iconImgView.frame)+60, appWidth-kLeftPadding*2, 52.5);
    lineLab.frame=CGRectMake(CGRectGetMinX(phoneNumberTF.frame), CGRectGetMaxY(phoneNumberTF.frame)+1, appWidth-kLeftPadding*2, 1);
    CGFloat getCodeBtnWidth=[[SysUtils sharedSysUtils] widthOfString:getCodeBtn.titleLabel.text font:getCodeBtn.titleLabel.font height:52.5];
    noteCodeTF.frame=CGRectMake(CGRectGetMinX(phoneNumberTF.frame), CGRectGetMaxY(lineLab.frame), appWidth-kLeftPadding*2-getCodeBtnWidth-30, 52.5);
    verticalLineLab.frame=CGRectMake(CGRectGetMaxX(noteCodeTF.frame)+18, CGRectGetMaxY(lineLab.frame)+15.75, 1, 21);
    getCodeBtn.frame=CGRectMake(CGRectGetMaxX(verticalLineLab.frame)+11,CGRectGetMinY(noteCodeTF.frame), getCodeBtnWidth,52.5);
    lineLab2.frame=CGRectMake(CGRectGetMinX(phoneNumberTF.frame), CGRectGetMaxY(noteCodeTF.frame), appWidth-kLeftPadding*2, 1);
    
    if (loginPath==NewBJPasswordLoginPath) {
        loginBtn.frame=CGRectMake(kLeftPadding, CGRectGetMaxY(passwordPathLineLab2.frame)+30, appWidth-kLeftPadding*2, 44);
    }else{
        loginBtn.frame=CGRectMake(kLeftPadding, CGRectGetMaxY(lineLab2.frame)+30, appWidth-kLeftPadding*2, 44);
    }
    
    CGFloat changeLoginPathBtnWidth=0;
    if (loginPath==NewBJAuthCodeLoginPath) {
        changeLoginPathBtnWidth=[[SysUtils sharedSysUtils] widthOfString:GetLocalResStr(@"dk_use_password_login_hint") font:changeLoginPathBtn.titleLabel.font height:22.5];
        changeLoginPathBtn.frame=CGRectMake((appWidth-changeLoginPathBtnWidth)/2, appHeight-22.5-30, changeLoginPathBtnWidth, 22.5);
    }else{
        changeLoginPathBtnWidth=[[SysUtils sharedSysUtils] widthOfString:GetLocalResStr(@"dk_use_code_login_hint") font:changeLoginPathBtn.titleLabel.font height:22.5];
        changeLoginPathBtn.frame=CGRectMake((appWidth-changeLoginPathBtnWidth)/2, appHeight-22.5-30, changeLoginPathBtnWidth, 22.5);
    }
}
-(void)back {
    [self.navigationController popViewControllerAnimated:YES];
}
//登录方式
-(void)changeLoginPath:(UIButton *)sender{
    if (loginPath==NewBJAuthCodeLoginPath) {
        loginPath=NewBJPasswordLoginPath;
    }else{
        loginPath=NewBJAuthCodeLoginPath;
    }
    [self updateLoginInterface];
    [self updateloginBtn];
}
//更新登录界面
-(void)updateLoginInterface{
    if (loginPath==NewBJPasswordLoginPath) {
        jobNumberTF.hidden=NO;
        passwordTF.hidden=NO;
        seeButton.hidden=NO;
        passwordPathLineLab.hidden=NO;
        passwordPathLineLab2.hidden=NO;
        phoneNumberTF.hidden=YES;
        noteCodeTF.hidden=YES;
        verticalLineLab.hidden=YES;
        getCodeBtn.hidden=YES;
        lineLab.hidden=YES;
        lineLab2.hidden=YES;
        [changeLoginPathBtn setTitle:GetLocalResStr(@"dk_use_code_login_hint") forState:UIControlStateNormal];
        CGFloat changeLoginPathBtnWidth=[[SysUtils sharedSysUtils] widthOfString:GetLocalResStr(@"dk_use_code_login_hint") font:changeLoginPathBtn.titleLabel.font height:22.5];
        CGRect changeLoginPathFrm=changeLoginPathBtn.frame;
        changeLoginPathFrm.size.width=changeLoginPathBtnWidth;
        changeLoginPathFrm.origin.x=(appWidth-changeLoginPathBtnWidth)/2;
        changeLoginPathBtn.frame=changeLoginPathFrm;
    }else{
        jobNumberTF.hidden=YES;
        passwordTF.hidden=YES;
        seeButton.hidden=YES;
        passwordPathLineLab.hidden=YES;
        passwordPathLineLab2.hidden=YES;
        phoneNumberTF.hidden=NO;
        noteCodeTF.hidden=NO;
        verticalLineLab.hidden=NO;
        getCodeBtn.hidden=NO;
        lineLab.hidden=NO;
        lineLab2.hidden=NO;
        [changeLoginPathBtn setTitle:GetLocalResStr(@"dk_use_password_login_hint") forState:UIControlStateNormal];
        CGFloat changeLoginPathBtnWidth=[[SysUtils sharedSysUtils] widthOfString:GetLocalResStr(@"dk_use_password_login_hint") font:changeLoginPathBtn.titleLabel.font height:22.5];
        CGRect changeLoginPathFrm=changeLoginPathBtn.frame;
        changeLoginPathFrm.size.width=changeLoginPathBtnWidth;
        changeLoginPathFrm.origin.x=(appWidth-changeLoginPathBtnWidth)/2;
        changeLoginPathBtn.frame=changeLoginPathFrm;
    }
}
-(void)change:(UIButton *) sender {
    sender.selected = !sender.selected;
    passwordTF.secureTextEntry = !passwordTF.secureTextEntry ; //是否以密码形式显示
    NSString *text = passwordTF.text;
    passwordTF.text = @"";
    passwordTF.text = text;
}
//获取验证码
-(void)getNoteCodeAuthCode{
    NSString *port = DK_PORT;
    NSString *url=nil;
    NSString *apiStr=nil;
    url=@"/api/v1/auth_code/login/send";
    if([port isEqualToString: @"443"] || [port isEqualToString:@"80"]) {
        apiStr = [NSString stringWithFormat:@"%@%@", DK_URL,url];
    } else {
        apiStr = [NSString stringWithFormat:@"%@:%@%@",DK_URL,DK_PORT,url];
    }
    NSDictionary *param=[NSDictionary dictionary];
    phoneNumber=phoneNumberTF.text;
    param = @{@"phone":phoneNumber};
}
-(void)timerFired:(NSTimer *)_timer {
    NSLog(@"num = %d",_count);
    if (_count > 0) {
        //        if (self.state==JSHandleFirstLogin) {
        phoneNumberTF.textColor = [UIColor dk_colorWithHexString:mainTextGrayColor];
        phoneNumberTF.enabled=NO;
        //        }
        _count -=1;
        NSString *str = [NSString stringWithFormat:GetLocalResStr(@"dk_send_noteCode_count"), _count];
        [getCodeBtn setTitle:str forState:UIControlStateNormal];
        getCodeBtn.enabled=NO;
        [getCodeBtn setTitleColor:[UIColor dk_colorWithHexString:mainTextGrayColor] forState:UIControlStateNormal];
        [self updateGetCodeBtnWidth];
    }else{
        //        if(loginPath==NewBJAuthCodeLoginPath) {
        phoneNumberTF.enabled=YES;
        phoneNumberTF.textColor = [UIColor dk_colorWithHexString: mainBlackColor];
        //        }
        [timer invalidate];
        timer = nil;
        [getCodeBtn setTitle:GetLocalResStr(@"dk_send_noteCode") forState:UIControlStateNormal];
        getCodeBtn.enabled = YES;
        [getCodeBtn setTitleColor:[UIColor dk_colorWithHexString:mainThemeColor] forState:UIControlStateNormal];
        [self updateGetCodeBtnWidth];
    }
}
-(void)changeField:(UITextField *)textfield{
    if (textfield==jobNumberTF) {
        [passwordTF becomeFirstResponder];
    }
    if (textfield==passwordTF) {
        if(passwordTF.text.length>0&&jobNumberTF.text.length>0){
            [self login];
        }
    }
    if (textfield==phoneNumberTF) {
        [noteCodeTF becomeFirstResponder];
    }
    if(textfield==noteCodeTF){
        if(noteCodeTF.text.length>=6&&phoneNumberTF.text.length>0){
            [self login];
        }
    }
}

- (void)login{
    [self setLoginBtnBackgroundColorWithEnable:NO];
    [jobNumberTF resignFirstResponder];
    [passwordTF resignFirstResponder];
    [phoneNumberTF resignFirstResponder];
    [noteCodeTF resignFirstResponder];
    if (loginPath==NewBJPasswordLoginPath) {
        [self passwordLogin];
    }else{
        [self authCodeLogin];
    }
}

//手机验证码登录（首次登录、手机验证码直接登录）
-(void)authCodeLogin{
    phoneNumber=phoneNumberTF.text;
}
//密码登录
-(void)passwordLogin{
    self.jobNum=jobNumberTF.text;
    NSString *port = DK_PORT;
    NSString *url=@"/api/v1/users/check/current_account";
    NSString *apiStr=[NSString stringWithFormat:@"%@%@", BASE_URL,url];
    NSDictionary *param = @{@"login_name":self.jobNum,@"no_phone_validation":@(true)};
    
}
-(void)verifyDeviceFirstLogin{
}
-(void)checkAccountAccessible{
}

#pragma mark UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if(textField==jobNumberTF){
        passwordPathLineLab.backgroundColor=[UIColor dk_colorWithHexString:mainThemeColor];
    }else if (textField==passwordTF){
        passwordPathLineLab2.backgroundColor=[UIColor dk_colorWithHexString:mainThemeColor];
    }else if (textField==phoneNumberTF){
        lineLab.backgroundColor=[UIColor dk_colorWithHexString:mainThemeColor];
    }else if (textField==noteCodeTF){
        lineLab2.backgroundColor=[UIColor dk_colorWithHexString:mainThemeColor];
    }
    return YES;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    if(textField==jobNumberTF){
        passwordPathLineLab.backgroundColor=[UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:1];
    }else if (textField==passwordTF){
        passwordPathLineLab2.backgroundColor=[UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:1];
    }else if (textField==phoneNumberTF){
        lineLab.backgroundColor=[UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:1];
    }else if (textField==noteCodeTF){
        lineLab2.backgroundColor=[UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:1];
    }
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    [self updateloginBtn];
    if (!toBeString||[toBeString isKindOfClass:[NSNull class]]) {
        return NO;
    }
    return YES;
}

- (void)textValueDidChanged:(UITextField *)textfiled{
    [self textFieldDidChange:textfiled];
}

-(void)textFieldDidChange:(UITextField *)textField {
    if(textField==jobNumberTF){
        //        if (jobNumberTF.text.length >6&&![[textField.text lowercaseString] containsString:@"getcon"]){
        //            jobNumberTF.text = [jobNumberTF.text substringToIndex:6];
        //        }
    }else if (textField==passwordTF) {
        if(passwordTF.text.length>18){
            passwordTF.text = [passwordTF.text substringToIndex:18];
        }
    }else if (textField==phoneNumberTF) {
        if (phoneNumberTF.text.length >11){
            phoneNumberTF.text = [phoneNumberTF.text substringToIndex:11];
        }
        [self updateGetCodeBtn];
    }else if(textField==noteCodeTF){
        if (noteCodeTF.text.length >6){
            noteCodeTF.text = [noteCodeTF.text substringToIndex:6];
        }
    }
    [self updateloginBtn];
}
-(void)updateGetCodeBtnWidth{
    CGFloat getCodeBtnWidth;
    if (_count>0) {
        getCodeBtnWidth=[[SysUtils sharedSysUtils] widthOfString:[NSString stringWithFormat:GetLocalResStr(@"dk_send_noteCode_count"),60] font:getCodeBtn.titleLabel.font height:52.5];
        getCodeBtnWidth=getCodeBtnWidth+3;
    }else{
        getCodeBtnWidth=[[SysUtils sharedSysUtils] widthOfString:getCodeBtn.titleLabel.text font:getCodeBtn.titleLabel.font height:52.5];
    }
    noteCodeTF.frame=CGRectMake(CGRectGetMinX(phoneNumberTF.frame), CGRectGetMaxY(lineLab.frame), appWidth-kLeftPadding*2-getCodeBtnWidth-30, 52.5);
    verticalLineLab.frame=CGRectMake(CGRectGetMaxX(noteCodeTF.frame)+18, CGRectGetMaxY(lineLab.frame)+15.75, 1, 21);
    getCodeBtn.frame=CGRectMake(CGRectGetMaxX(verticalLineLab.frame)+11,CGRectGetMinY(noteCodeTF.frame), getCodeBtnWidth,52.5);
}
-(void)updateGetCodeBtn{
    if (phoneNumberTF.text&&![phoneNumberTF isKindOfClass:[NSNull class]]&&phoneNumberTF.text.length==11) {
        getCodeBtn.enabled = YES;
        [getCodeBtn setTitleColor:[UIColor dk_colorWithHexString:mainThemeColor] forState:UIControlStateNormal];
    }else{
        getCodeBtn.enabled = NO;
        [getCodeBtn setTitleColor:[UIColor dk_colorWithHexString:mainTextGrayColor] forState:UIControlStateNormal];
    }
}
-(void)updateloginBtn{
    if (loginPath==NewBJPasswordLoginPath) {
        if (passwordTF.text&&![passwordTF isKindOfClass:[NSNull class]]&&passwordTF.text.length>=6&&jobNumberTF.text&&![jobNumberTF isKindOfClass:[NSNull class]]&&jobNumberTF.text.length!=0) {
            [self setLoginBtnBackgroundColorWithEnable:YES];
            [loginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }else{
            [loginBtn setTitleColor:[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:0.4] forState:UIControlStateNormal];
            [self setLoginBtnBackgroundColorWithEnable:NO];
        }
    }else{
        if (noteCodeTF.text&&![noteCodeTF isKindOfClass:[NSNull class]]&&noteCodeTF.text.length==6&&phoneNumberTF.text&&![phoneNumberTF isKindOfClass:[NSNull class]]&&phoneNumberTF.text.length==11) {
            [self setLoginBtnBackgroundColorWithEnable:YES];
            [loginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }else{
            [loginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [self setLoginBtnBackgroundColorWithEnable:NO];
        }
    }
    
}
//alertcontroller 只有一个action
-(void)showAlertWithTitle:(NSString *)title
                  message:(NSString *)message
                     type:(UIAlertControllerStyle)type
              actionTitle:(NSString *)actionTitle
            actionHandler:(void(^)(void))handler {
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self hiddenKey];
}
-(void)hiddenKey {
    [self.view.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [((UIView *)obj) resignFirstResponder];
    }];
}
-(void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
        if (isIpad) {
            [self reDrawRect];
            _offset=0.1;
            [self hiddenKey];
        }
    } completion:nil];
}
-(void)dealloc{
#ifdef DK_MDM_ENABLE
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kMXMDMDissmissHUDNotification object:nil];
#endif
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"kFinishUploadLog" object:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)uploadLog {
//    [[MXKit shareMXKit] uploadLog:[NSString stringWithFormat:@"%@/api/v1/mobile/report", DK_URL]];
}

-(void)uploadLogFinish {
//    [SVProgressHUD showSuccessWithStatus:@"上传日志成功" duration:3.0f];
}

- (void)uploadLogTappedWithGesture:(UITapGestureRecognizer *)tapGesture
{
    //判断是不是连续6次点击同一个页签开启邮箱设置页面
    NSTimeInterval now = [[NSDate date] timeIntervalSince1970];
    static int count=0;
    if(now - lastTapTimeStamp <= 0.5) {
        count+=1;
        if (count==9) {
            [self uploadLog];
            return;
        }
    }else{
        count=0;
    }
    lastTapTimeStamp = now;
}

@end
