//
//  DKLoginViewController.h
//  DaKa
//
//  Created by 刁俊方 on 2020/1/5.
//  Copyright © 2020 刁俊方. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,NewBJLoginPath) {
    NewBJAuthCodeLoginPath = 0, //验证码登录
    NewBJPasswordLoginPath = 1,//密码登录
};

NS_ASSUME_NONNULL_BEGIN

@interface DKLoginViewController : UIViewController

@property (nonatomic, copy) NSString *jobNum;
@property (nonatomic, copy) NSString *phoneNum;
@property (nonatomic, copy) NSString *password;
@property (nonatomic,assign)BOOL isUpdatePhoneNumber; //是否上传手机号(首次登录没有手机号的情况)

@end

NS_ASSUME_NONNULL_END
