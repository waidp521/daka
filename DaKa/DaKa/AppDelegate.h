//
//  AppDelegate.h
//  DaKa
//
//  Created by 刁俊方 on 2020/1/4.
//  Copyright © 2020 刁俊方. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

