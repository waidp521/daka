//
//  UISegmentedControl+Theme.m
//  MXKit
//
//  Created by liyang on 2017/7/6.
//  Copyright © 2017年 minxing. All rights reserved.
//

#import "UISegmentedControl+Theme.h"
#import <objc/runtime.h>

@implementation UISegmentedControl (Theme)

- (NSString *)mx_configKey {
    return objc_getAssociatedObject(self, @selector(mx_configKey));
}

-(void)setMx_configKey:(NSString *)mx_configKey {
    objc_setAssociatedObject(self, @selector(mx_configKey), mx_configKey, OBJC_ASSOCIATION_COPY_NONATOMIC);
    [self mx_setTintColor];
}

- (MEColorPicker)mx_tintColor {
    return ^() {
        NSDictionary *themeDic = [METhemeManager sharedThemeManager].currentThemeConfig;
        NSString *colorHexStr = themeDic[@"UISegmentedControl"][[self mx_configKey]][@"tintColor"];
        return [UIColor dk_colorWithHexString:colorHexStr];
    };
}

-(void)mx_setTintColor {
    MEColorPicker picker = [self mx_tintColor];
    self.tintColor = picker();
    [self.pickers setValue:[picker copy] forKey:@"setTintColor"];
}

@end
