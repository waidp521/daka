//
//  UISegmentedControl+Theme.h
//  MXKit
//
//  Created by liyang on 2017/7/6.
//  Copyright © 2017年 minxing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UISegmentedControl (Theme)

@property (nonatomic, strong,nullable) NSString * mx_configKey;

@property (nonatomic,copy)MEColorPicker mx_tintColor;

-(void)mx_setTintColor;

@end
