//
//  UIView+Theme.m
//  neighborhood
//
//  Created by ss on 16/1/18.
//  Copyright © 2016年 iYaYa. All rights reserved.
//

#import "UIView+Theme.h"
#import "NSObject+Theme.h"
#import <objc/runtime.h>
@implementation UIView (Theme)
- (MEColorPicker)mx_backgroundColor{
    return objc_getAssociatedObject(self, @selector(mx_backgroundColor));
}
- (void)setMx_backgroundColor:(MEColorPicker)mx_backgroundColor{
    objc_setAssociatedObject(self, @selector(mx_backgroundColor), mx_backgroundColor, OBJC_ASSOCIATION_COPY_NONATOMIC);
    self.backgroundColor = mx_backgroundColor();
    [self.pickers setValue:[mx_backgroundColor copy] forKey:@"setBackgroundColor:"];
}
@end
