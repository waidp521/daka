//
//  UINavigationBar+Theme.m
//  ThemeDemo
//
//  Created by ss on 16/1/13.
//  Copyright © 2016年 Yasin. All rights reserved.
//

#import "UINavigationBar+Theme.h"
#import "ThemeProperties.h"
#import "METhemeManager.h"
#import "NSObject+Theme.h"
#import "UIImage+Theme.h"
#import <objc/runtime.h>
@interface UINavigationBar ()

@property (nonatomic, strong) NSMutableDictionary<NSString *, id> *pickers;

@end
@implementation UINavigationBar (Theme)
- (void)mx_setShadowImageForType:(NavBarType)navType{
    MEColorPicker picker = [self mx_navBarShadowImageColor];
    self.shadowImage = [UIImage mx_createImageFromColor:picker];
    [self.pickers setValue:[picker copy] forKey:@"setShadowImage:"];
}
- (void)mx_setBackgroundImageForBarMetrics:(UIBarMetrics)barMetrics WithType:(NavBarType)navType{
    NSString *key = [NSString stringWithFormat:@"%@", @(barMetrics)];
    MEColorPicker colorPicker = [self mx_NavBarBackgroundColor];
    [self setBackgroundImage:[UIImage mx_createImageFromColor:colorPicker] forBarMetrics:barMetrics];
    id dictionary = [self.pickers valueForKey:key];
    if (!dictionary || ![dictionary isKindOfClass:[NSMutableDictionary class]]) {
        dictionary = [[NSMutableDictionary alloc] init];
    }
    [dictionary setValue:[colorPicker copy] forKey:NSStringFromSelector(@selector(setBackgroundImage:forBarMetrics:))];
    [self.pickers setValue:dictionary forKey:key];
}

-(void)mx_setTitleColor {
    MEColorPicker picker = [self mx_navBarTitleColor];
    NSMutableDictionary *titleAttrDic = [[self titleTextAttributes] mutableCopy];
    [titleAttrDic setObject:picker() forKey:NSForegroundColorAttributeName];
    [self setTitleTextAttributes:titleAttrDic];
    [self.pickers setValue:[picker copy] forKey:@"setTitleColor"];
}

-(void)mx_setBarTintColor {
    MEColorPicker picker = [self mx_navBarBarTintColor];
    self.barTintColor = picker();
    [self.pickers setValue:[picker copy] forKey:@"setBarTintColor"];
}

-(void)mx_setBarStyle {
    NSString *barStyle = [self mx_navBarStyle];
    if([barStyle isEqualToString:@"UIBarStyleBlack"]) {
        self.barStyle = UIBarStyleBlack;
    } else if([barStyle isEqualToString:@"UIBarStyleDefault"]) {
        self.barStyle = UIBarStyleDefault;
    }
}


- (void)changeTheme{
    [self.pickers enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            NSDictionary<NSString *, MEPicker> *dictionary = (NSDictionary *)obj;
            [dictionary enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull selector, MEPicker  _Nonnull picker, BOOL * _Nonnull stop) {
                UIBarMetrics state = [key integerValue];
                [UIView animateWithDuration:METhemeAnimationDuration
                                 animations:^{
                                     if ([selector isEqualToString:NSStringFromSelector(@selector(setBackgroundImage:forBarMetrics:))]) {
                                         UIImage *resultImage = ([UIImage mx_createImageFromColor:(MEColorPicker)picker]);
                                         [self setBackgroundImage:resultImage forBarMetrics:state];
                                     }
                                 }];
            }];
        } else {
            if ([key isEqualToString:NSStringFromSelector(@selector(setShadowImage:))]) {
                MEColorPicker picker = obj;
                self.shadowImage = [UIImage mx_createImageFromColor:picker];
            } else if([key isEqualToString:@"setTitleColor"]) {
                MEPicker picker = (MEPicker)obj;
                NSMutableDictionary *titleAttrDic = [[self titleTextAttributes] mutableCopy];
                [titleAttrDic setObject:picker() forKey:NSForegroundColorAttributeName];
                [self setTitleTextAttributes:titleAttrDic];
            } else if([key isEqualToString:@"setBarTintColor"]) {
                MEPicker picker = (MEPicker)obj;
                [self setBarTintColor:picker()];
            } else{
                SEL sel = NSSelectorFromString(key);
                MEPicker picker = (MEPicker)obj;
                id result = picker();
                [UIView animateWithDuration:METhemeAnimationDuration
                                 animations:^{
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                                     [self performSelector:sel withObject:result];
#pragma clang diagnostic pop
                                 }];
            }
            
            
        }
    }];
}
#pragma mark - GET Color
- (NSString *)navBarTypeStr{
    return @"NavBarDefault";
}

#pragma mark BackgroundImageColor
- (MEColorPicker)mx_NavBarBackgroundColor{
    return ^() {
        NSString *colorHexStr = [METhemeManager sharedThemeManager].currentThemeConfig[@"UINavigationBar"][[self navBarTypeStr]][@"backgroundImageColor"];
        return [UIColor dk_colorWithHexString:colorHexStr];
    };
}
#pragma mark BarShadowImageColor
- (MEColorPicker)mx_navBarShadowImageColor{
    return ^() {
        NSString *colorHexStr = [METhemeManager sharedThemeManager].currentThemeConfig[@"UINavigationBar"][[self navBarTypeStr]][@"shadowImageColor"];
        return [UIColor dk_colorWithHexString:colorHexStr];
    };
}

#pragma mark TitleColor
- (MEColorPicker)mx_navBarTitleColor{
    return ^() {
        NSString *colorHexStr = [METhemeManager sharedThemeManager].currentThemeConfig[@"UINavigationBar"][[self navBarTypeStr]][@"titleLabelColor"];
        return [UIColor dk_colorWithHexString:colorHexStr];
    };
}

- (MEColorPicker)mx_navBarBarTintColor {
    return ^() {
        NSString *colorHexStr = [METhemeManager sharedThemeManager].currentThemeConfig[@"UINavigationBar"][[self navBarTypeStr]][@"barTintColor"];
        return [UIColor dk_colorWithHexString:colorHexStr];
    };
}

-(NSString *)mx_navBarStyle {
    return [METhemeManager sharedThemeManager].currentThemeConfig[@"UINavigationBar"][[self navBarTypeStr]][@"barStyle"];
}


@end
