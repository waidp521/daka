//
//  UINavigationBar+Theme.h
//  ThemeDemo
//
//  Created by ss on 16/1/13.
//  Copyright © 2016年 Yasin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+Theme.h"
typedef NS_ENUM(NSUInteger,NavBarType) {
    NavBarDefault = 0,
    NavBarLevel1
};
@interface UINavigationBar (Theme)
- (void)mx_setBackgroundImageForBarMetrics:(UIBarMetrics)barMetrics WithType:(NavBarType)navType;
- (void)mx_setShadowImageForType:(NavBarType)navType;
-(void)mx_setTitleColor;
-(void)mx_setBarTintColor;
-(void)mx_setBarStyle;
#pragma mark TitleColor
- (MEColorPicker)mx_navBarTitleColor;
- (MEColorPicker)mx_navBarBarTintColor;
@end
