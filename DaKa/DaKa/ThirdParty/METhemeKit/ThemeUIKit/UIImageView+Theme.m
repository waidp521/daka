//
//  UIImageView+Theme.m
//  ThemeDemo
//
//  Created by ss on 16/1/12.
//  Copyright © 2016年 Yasin. All rights reserved.
//

#import "UIImageView+Theme.h"
#import "NSObject+Theme.h"
#import <objc/runtime.h>

@implementation UIImageView (Theme)
- (instancetype)mx_initWithImage:(MEImagePicker)imagePicker{
    if ([self initWithImage:imagePicker()]) {
        [self.pickers setObject:[imagePicker copy] forKey:@"setImage:"];
        return self;
    }
    return nil;
}
-(MEImagePicker)mx_image{
    return objc_getAssociatedObject(self, @selector(mx_image));
}

-(void)setMx_image:(MEImagePicker)mx_image{
    objc_setAssociatedObject(self, @selector(mx_image), mx_image, OBJC_ASSOCIATION_COPY_NONATOMIC);
    self.image = mx_image();
    [self.pickers setObject:[mx_image copy] forKey:@"setImage:"];
}

@end




