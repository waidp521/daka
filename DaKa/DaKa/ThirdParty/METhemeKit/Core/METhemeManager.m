//
//  METhemeManager.m
//  ThemeDemo
//
//  Created by ss on 16/1/12.
//  Copyright © 2016年 Yasin. All rights reserved.
//

#import "METhemeManager.h"
#import <UIKit/UIKit.h>
#import "ThemeProperties.h"
#define BlackColorHex @"000000"

static METhemeManager *instance = nil;

@interface METhemeManager ()

@end

@implementation METhemeManager
//+ (void)load{
//    [super load];
//    [METhemeManager sharedThemeManager];
//}
+ (METhemeManager *)sharedThemeManager {
    @synchronized(self){//为了确保多线程情况下，仍然确保实体的唯一性
        if (!instance) {
            instance = [[self alloc] init]; //确保使用同一块内存地址
        }
    }
    return instance;
}
+(id)allocWithZone:(NSZone *)zone{
    @synchronized(self){// //为了确保多线程情况下，仍然确保实体的唯一性
        if (!instance) {
            instance = [super allocWithZone:zone]; //确保使用同一块内存地址
            return instance;
        }
    }
    return nil;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.themeName = @"ThemeDefault";
    }
    return self;
}
- (id)copyWithZone:(NSZone *)zone;{
    return self; //确保copy对象也是唯一
}
#pragma mark - 获取配置
#pragma mark - 获取图片名字前缀
+ (NSString *)getImageNamePrefix{
    return [METhemeManager sharedThemeManager].imageNamePrefix;
}







@end
