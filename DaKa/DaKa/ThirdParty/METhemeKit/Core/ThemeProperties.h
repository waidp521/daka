//
//  ThemeProperties.h
//  neighborhood
//
//  Created by Yasin on 16/1/17.
//  Copyright © 2016年 Yasin. All rights reserved.
//

#ifndef ThemeProperties_h
#define ThemeProperties_h

#define kMEThemeChangeNotification @"kMEThemeChangeNotificationName"
#define METhemeAnimationDuration 0.3

static NSString * ThemeTypeKey = @"ThemeType";


static NSString * ThemeColorMode_Default = @"ThemeColorMode_Default";
static NSString * ThemeColorMode_Default_Highlight = @"ThemeColorMode_Default_Highlight";


// Config button
static NSString *ThemeMode_Button_Private_Message_Quit = @"ThemeMode_Button_Private_Message_Quit";
static NSString *ThemeMode_Button_Private_Message_EnterChat = @"ThemeMode_Button_Private_Message_EnterChat";
static NSString *ThemeMode_Button_Private_Message_Subscribe = @"ThemeMode_Button_Private_Message_Subscribe";
static NSString *ThemeMode_Button_Private_Message_Unsubscribe = @"ThemeMode_Button_Private_Message_Unsubscribe";

static NSString *ThemeMode_Button_Select_Person_Confirm = @"ThemeMode_Button_Select_Person_Confirm";

static NSString *ThemeMode_Button_Group_Delete = @"ThemeMode_Button_Group_Delete";
static NSString *ThemeMode_Button_Group_Quite = @"ThemeMode_Button_Group_Quite";

static NSString *ThemeMode_Button_Circle_Function_Item = @"ThemeMode_Button_Circle_Function_Item";


static NSString *ThemeMode_Button_AppCenter_Detail_Install = @"ThemeMode_Button_AppCenter_Detail_Install";
static NSString *ThemeMode_Button_AppCenter_Detail_Enter = @"ThemeMode_Button_AppCenter_Detail_Enter";

static NSString *ThemeMode_Button_PersonInfo_Action_Add = @"ThemeMode_Button_PersonInfo_Action_Add";
static NSString *ThemeMode_Button_PersonInfo_Action_Delete = @"ThemeMode_Button_PersonInfo_Action_Delete";
static NSString *ThemeMode_Button_PersonInfo_Action_Message = @"ThemeMode_Button_PersonInfo_Action_Message";
static NSString *ThemeMode_Button_PersonInfo_Action_Follow = @"ThemeMode_Button_PersonInfo_Action_Follow";


static NSString *ThemeMode_Button_Email_Login = @"ThemeMode_Button_Email_Login";


static NSString * ThemeMode_Button_NavBarRight = @"ThemeMode_Button_NavBarRight"; //







static NSString * ThemeMode_Button_Sure = @"ThemeMode_Button_SureButton"; //
static NSString * ThemeMode_Button_NoBackgroundImage_SureButton = @"ThemeMode_Button_NoBackgroundImage_SureButton";
#define ThemeMode_Button_Sure_CapInsets UIEdgeInsetsMake(5, 5, 5, 5)
//快递，地址选择器，上部地址选择指示
static NSString * ThemeMode_Button_ExpressAddressSele = @"ThemeMode_Button_ExpressAddressSele";
//时间选择器的取消确定按钮
static NSString * ThemeModel_Button_PickerView = @"ThemeModel_Button_PickerView";



static NSString *ThemeMode_SegmentControl_Private_Message_FileImage = @"ThemeMode_SegmentControl_Private_Message_FileImage";

// color

// 工具租借， 我的工具租借 取消按钮
static NSString * ThemeMode_Color_ToolRental_MyToolRental_CancelButton = @"ThemeMode_Color_ToolRental_MyToolRental_CancelButton";
static NSString * ThemeMode_Color_LifeNumber_Label_CategoryItemHot = @"ThemeMode_Color_LifeNumber_Label_CategoryItemHot";

// page control
static NSString * ThemeMode_Color_UserGuide_PageControl_PageIndicatorColor = @"ThemeMode_Color_UserGuide_PageControl_PageIndicatorColor";
static NSString * ThemeMode_Color_UserGuide_PageControl_CurrentPageIndicatorColor = @"ThemeMode_Color_UserGuide_PageControl_CurrentPageIndicatorColor";

// chat
static NSString * ThemeMode_Color_Chat_MyMessage_ContentTextColor = @"ThemeMode_Color_Chat_MyMessage_ContentTextColor";

static NSString * ThemeMode_Color_BBS_Reply_Vest_Select_BackgroundColor = @"ThemeMode_Color_BBS_Reply_Vest_Select_BackgroundColor";

// StatusBarStyle
static NSString * ThemeMode_StatusBarStyle = @"StatusBarStyle";
static NSString * ThemeMode_StatusBarStyle_DefaultStyle = @"Default";
static NSString * ThemeMode_StatusBarStyleDefault = @"UIStatusBarStyleDefault";
static NSString * ThemeMode_StatusBarStyleLightContent = @"UIStatusBarStyleLightContent";

#endif /* ThemeProperties_h */
