//
//  CALayer+Theme.m
//  neighborhood
//
//  Created by 杨世昌 on 16/1/18.
//  Copyright © 2016年 iYaYa. All rights reserved.
//

#import "CALayer+Theme.h"
#import "NSObject+Theme.h"
#import <objc/runtime.h>
@implementation CALayer (Theme)

-(MECGColorPicker)mx_borderColor{
    return objc_getAssociatedObject(self, @selector(mx_borderColor));
}
-(void)setMx_borderColor:(MECGColorPicker)mx_borderColor {
    objc_setAssociatedObject(self, @selector(mx_borderColor), mx_borderColor, OBJC_ASSOCIATION_COPY_NONATOMIC);
    self.borderColor = mx_borderColor();
    
    [self.pickers setValue:[mx_borderColor copy] forKey:@"setBorderColor:"];
}

- (MECGColorPicker)mx_backgroundColor{
    return objc_getAssociatedObject(self, @selector(mx_backgroundColor));
}
- (void)setMx_backgroundColor:(MECGColorPicker)mx_backgroundColor{
    objc_setAssociatedObject(self, @selector(mx_backgroundColor), mx_backgroundColor, OBJC_ASSOCIATION_COPY_NONATOMIC);
    self.backgroundColor = mx_backgroundColor();
    [self.pickers setValue:[mx_backgroundColor copy] forKey:@"setBackgroundColor:"];
}
@end
