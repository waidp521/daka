//
//  DDLogFileManagerDefault.m
//  MXKit
//
//  Created by liyang on 16/6/16.
//  Copyright © 2016年 minxing. All rights reserved.
//

#import "DDLogFileManagerNewDefault.h"

@implementation DDLogFileManagerNewDefault
- (NSString *)newLogFileName {
    NSDateFormatter *dateFormatter = [super logFileDateFormatter];
    NSString *formattedDate = [dateFormatter stringFromDate:[NSDate date]];
    
    return [NSString stringWithFormat:@"%@.log", formattedDate];
}

- (BOOL)isLogFile:(NSString *)fileName {
    BOOL hasProperSuffix = [fileName hasSuffix:@".log"];
    BOOL hasProperDate = NO;
    
    if (hasProperSuffix) {
        NSUInteger lengthOfMiddle = fileName.length - @".log".length;
        
        // Date string should have at least 16 characters - " 2013-12-03 17-14"
        if (lengthOfMiddle >= 16) {
            NSRange range = NSMakeRange(0, lengthOfMiddle);
            
            NSString *middle = [fileName substringWithRange:range];
            NSArray *components = [middle componentsSeparatedByString:@" "];
            
            // When creating logfile if there is existing file with the same name, we append attemp number at the end.
            // Thats why here we can have three or four components. For details see createNewLogFile method.
            //
            // Components:
            //     "", "2013-12-03", "17-14"
            // or
            //     "", "2013-12-03", "17-14", "1"
            if (components.count == 2 || components.count == 3) {
                NSString *dateString = [NSString stringWithFormat:@"%@ %@", components[0], components[1]];
                NSDateFormatter *dateFormatter = [self logFileDateFormatter];
                
                NSDate *date = [dateFormatter dateFromString:dateString];
                
                if (date) {
                    hasProperDate = YES;
                }
            }
        }
    }
    BOOL result = (hasProperDate && hasProperSuffix);
    return result;
}
@end
