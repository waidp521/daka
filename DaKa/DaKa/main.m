//
//  main.m
//  DaKa
//
//  Created by 刁俊方 on 2020/1/4.
//  Copyright © 2020 刁俊方. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
