//
//  UrlRequestSettingVO.h
//  DaKa
//
//  Created by 刁俊方 on 2020/1/5.
//  Copyright © 2020 刁俊方. All rights reserved.
//

#import "PersistentObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface UrlRequestSettingVO : PersistentObject

@property (nonatomic, assign) int reconnectionTimes;//网络请求失败重连次数
@property (nonatomic, assign) BOOL mustBeSend;//发送失败的情况下 网络好的时候重新发送
@property (nonatomic, assign) BOOL hiddenError;//YES的情况下会吃掉报错信息

@end

NS_ASSUME_NONNULL_END
