//
//  UrlRequestVO.h
//  DaKa
//
//  Created by 刁俊方 on 2020/1/5.
//  Copyright © 2020 刁俊方. All rights reserved.
//

#import "PersistentObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface UrlRequestVO : PersistentObject

@property (nonatomic, copy) NSString * url;
@property (nonatomic, copy) NSString * method;
@property (nonatomic, strong) NSDictionary * params;
@property (nonatomic, strong) NSURLRequest * request;

+ (instancetype)requestWithUrlRequest:(NSURLRequest *)request;
//-(void) convertToRealmDBPersistance:(WBRequestRealmVO*)realmObj;
//+(instancetype) convertFromRealmDBPersistance:(WBRequestRealmVO*)realmObj;

@end

NS_ASSUME_NONNULL_END
