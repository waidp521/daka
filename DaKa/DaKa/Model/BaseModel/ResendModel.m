////
////  ResendModel.m
////  DaKa
////
////  Created by 刁俊方 on 2020/1/5.
////  Copyright © 2020 刁俊方. All rights reserved.
////
//
//#import "ResendModel.h"
//#import "UrlRequestVO.h"
//
//@implementation ResendModel
//
//+ (void)saveRequest:(NSURLSessionTask *)sessionTask Method:(NSString *)method Url:(NSString *)url Params:(NSDictionary *)params{
//
//    UrlRequestVO * requestVO = [[UrlRequestVO alloc] init];
//    requestVO.method = method;
//    requestVO.url = url;
//    requestVO.params = params;
//    requestVO.request = sessionTask.currentRequest;
//    [self updateRequestToDB:requestVO];
//}
//
//+ (void)deleteRequest:(NSURLSessionTask *)sessionTask Method:(NSString *)method Url:(NSString *)url Params:(NSDictionary *)params{
//
//    UrlRequestVO * requestVO = [[UrlRequestVO alloc] init];
//    requestVO.method = method;
//    requestVO.url = url;
//    requestVO.params = params;
//    requestVO.request = sessionTask.currentRequest;
//    [self deleteRequest:requestVO];
//
//}
//
//+ (void)resendAllSavedRequest{
//
//    NSArray * requestArray = [self queryAllRequest];
//    for (UrlRequestVO * request in requestArray) {
//        if (request.request) {
//            WBNewBaseModel * model = [[WBNewBaseModel alloc] init];
//            NSURLSessionDataTask * dataTask = [model.httpRequestManager dataTaskWithRequest:request.request uploadProgress:nil downloadProgress:nil completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
//                if (responseObject &&!error) {
//                    [self deleteRequest:request];
//                }
//            }];
//            [dataTask resume];
//        }else{
//            [self deleteRequest:request];
//        }
//    }
//}
//
//+(RLMRealm *)getRealmInstace {
//    RLMRealmConfiguration *config = [RLMRealmConfiguration defaultConfiguration];
//    config.fileURL = [[FileUtils sharedFileUtils] getDBPath];
//    config.schemaVersion = DB_VERSION;
//    config.shouldCompactOnLaunch = ^BOOL(NSUInteger totalBytes, NSUInteger bytesUsed) {
//        NSUInteger fiveHundredKB = 500 * 1024 * 1024;
//        BOOL result=(totalBytes > fiveHundredKB) && (bytesUsed / totalBytes) < 0.5;
//        return result;
//    };
//    NSString *dbKey = [WBAppDelegate shareDelegate].dbKey;
//    dbKey = [NSString stringWithFormat:@"%@%@%@%@", dbKey,dbKey,dbKey,dbKey];
//    NSData *keyData = [WBSysUtil dataFromHexString:dbKey];
//    config.encryptionKey = keyData;
//    NSError *error = nil;
//    RLMRealm *realm = [RLMRealm realmWithConfiguration:config error:&error];
//
//    NSAssert(realm != nil, @"realm can not be null");
//
//    return realm;
//}
//
//+(void) updateRequestToDB:(WBUrlRequestVO*)request{
//
//    @autoreleasepool {
//        RLMRealm *realm = [self getRealmInstace];
//        if (!realm.inWriteTransaction) {
//            [realm beginWriteTransaction];
//        }
//
//        WBRequestRealmVO * realmObj = [[WBRequestRealmVO alloc] init];
//        [request convertToRealmDBPersistance:realmObj];
//
//        [WBRequestRealmVO createOrUpdateInRealm:realm withValue:realmObj];
//        [realm commitWriteTransaction];
//
//    }
//}
//
//+(NSArray*) queryAllRequest{
//    NSPredicate *pred = [NSPredicate predicateWithFormat:@" current_userId=%d",[WBUserVO userVO].ID];
//    RLMRealm *realm = [self getRealmInstace];
//    RLMResults *realmRequests = [WBRequestRealmVO objectsInRealm:realm withPredicate:pred];
//
//    if(realmRequests && realmRequests.count > 0) {
//        NSMutableArray * result = [NSMutableArray array];
//        for (WBRequestRealmVO * realmVO in realmRequests) {
//
//            WBUrlRequestVO * requestVO = [WBUrlRequestVO convertFromRealmDBPersistance:realmVO];
//
//            if(requestVO) {
//                [result addObject:requestVO];
//            }
//        }
//
//        return result;
//    }
//    return nil;
//}
//
//
//+ (void)deleteRequest:(WBUrlRequestVO *)request{
//    RLMRealm *realm = [self getRealmInstace];
//
//    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:request.params options:NSJSONWritingPrettyPrinted error:nil];
//    NSString * jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//    NSString * primaryKey = [NSString stringWithFormat:@"%d-%@-%@-%@",[WBUserVO userVO].ID, request.method,request.url,jsonString];
//
//    NSPredicate *pred = [NSPredicate predicateWithFormat:@"current_userId=%d AND primaryKey=%@ ", [WBUserVO userVO].ID,primaryKey];
//    RLMResults *result = [WBRequestRealmVO objectsInRealm:realm withPredicate:pred];
//
//    NSError *realmError = nil;
//    [realm transactionWithBlock:^{
//        [realm deleteObjects:result];
//    } error:&realmError];
//
//}
//
//@end
