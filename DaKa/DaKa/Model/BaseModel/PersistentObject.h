//
//  PersistentObject.h
//  DaKa
//
//  Created by 刁俊方 on 2020/1/4.
//  Copyright © 2020 刁俊方. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PersistentObject : NSObject

+ (id)initWithDictionary:(NSDictionary *)data;
+ (NSArray *)initWithArray:(NSArray *)data;
+ (NSDictionary *)propertiesWithEncodedTypes;

@end

NS_ASSUME_NONNULL_END
