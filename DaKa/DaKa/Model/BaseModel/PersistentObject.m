//
//  PersistentObject.m
//  DaKa
//
//  Created by 刁俊方 on 2020/1/4.
//  Copyright © 2020 刁俊方. All rights reserved.
//

#import <objc/runtime.h>
#import "PersistentObject.h"

@implementation PersistentObject

+ (NSArray *)initWithArray:(NSArray *)data
{
    if(!data || ![data isKindOfClass:[NSArray class]] || data.count == 0)
        return nil;
    
    id objectsArray = [NSMutableArray array];
    for (id obj in data) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
            [objectsArray addObject:[self initWithDictionary:obj]];
            [pool release];
        }
        
    }
    return objectsArray;
}


+ (id)initWithDictionary:(NSDictionary *)data
{
    id newObject = [[[[self class] alloc] init] autorelease];
    NSDictionary *theProps = [[self class] propertiesWithEncodedTypes];
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    for (NSString *prop in [theProps allKeys]) {
        NSString *realProp = prop;
        if ([@"ID" isEqualToString:prop]) {
            realProp = @"id";
        }
        if([@"_description" isEqualToString:prop]) {
            realProp = @"description";
        }
        NSObject *object = [data objectForKey:realProp];
        if (object && object != [NSNull null]) { // object 为 nil 或 null 时不赋值，使用对象初始化的默认值
            if([prop isEqualToString:@"integral"]){
                [newObject setValue:[NSNumber numberWithBool:YES] forKey:@"isShowIntergal"];
            }
            if([prop isEqualToString:@"merit_score"]){
                [newObject setValue:[NSNumber numberWithBool:YES] forKey:@"isShowMerit"];
            }
            [newObject setValue:object forKey:prop];
        }
    }
    [pool drain];
    return newObject;
}

+ (NSDictionary *)propertiesWithEncodedTypes
{
    // Recurse up the classes, but stop at NSObject. Each class only reports its own properties, not those inherited from its superclass
    NSMutableDictionary *theProps;
    
    if ([self superclass] != [NSObject class]) {
        theProps = (NSMutableDictionary *)[[self superclass] propertiesWithEncodedTypes];
    } else {
        theProps = [NSMutableDictionary dictionary];
    }
    
    unsigned int outCount;
    
#ifndef TARGET_OS_COCOTRON
    objc_property_t *propList = class_copyPropertyList([self class], &outCount);
#else
    NSArray *propList = [[self class] getPropertiesList];
    outCount = [propList count];
#endif
    int i;
    
    // Loop through properties and add declarations for the create
    for (i=0; i < outCount; i++)
    {
#ifndef TARGET_OS_COCOTRON
        objc_property_t oneProp = propList[i];
        NSString *propName = [NSString stringWithUTF8String:property_getName(oneProp)];
        NSString *attrs = [NSString stringWithUTF8String: property_getAttributes(oneProp)];
        // Read only attributes are assumed to be derived or calculated
        // See http://developer.apple.com/documentation/Cocoa/Conceptual/ObjCRuntimeGuide/Articles/chapter_8_section_3.html
        if ([attrs rangeOfString:@",R,"].location == NSNotFound)
        {
            NSArray *attrParts = [attrs componentsSeparatedByString:@","];
            if (attrParts != nil)
            {
                if ([attrParts count] > 0)
                {
                    NSString *propType = [[attrParts objectAtIndex:0] substringFromIndex:1];
                    [theProps setObject:propType forKey:propName];
                }
            }
        }
#else
        NSArray *oneProp = [propList objectAtIndex:i];
        NSString *propName = [oneProp objectAtIndex:0];
        NSString *attrs = [oneProp objectAtIndex:1];
        [theProps setObject:attrs forKey:propName];
#endif
    }
    
#ifndef TARGET_OS_COCOTRON
    free( propList );
#endif
    
    return theProps;
}

@end
