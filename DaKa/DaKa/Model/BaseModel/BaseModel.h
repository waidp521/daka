////
////  BaseModel.h
////  DaKa
////
////  Created by 刁俊方 on 2020/1/5.
////  Copyright © 2020 刁俊方. All rights reserved.
////
//
//#import <Foundation/Foundation.h>
//#import "AFNetworking.h"
//#import "MBProgressHUD.h"
//#import "BaseRequestSetting.h"
//
////定义每页请求数
//#define PAGESIZE    20
//
////获取配置文件中URL路径
//#define GETIBSTR(key) (GetApiStr(key))
//
///**
// *  模型回调结构体
// *
// *  @param result      结果集
// *  @param errorString 错误信息
// *
// */
//typedef void (^modelCallBack) (id result, NSString *errorString);
//
//typedef void (^appTunnelCallback) (NSURLResponse *response, id  responseObject,  NSError *error);
//
//typedef void (^progressCallback)(NSProgress *downloadProgress);
//
///**
// *  定义下载进度block
// *
// *  @param bytes              接收到的字节数
// *  @param totalBytes         总字节数
// *  @param totalBytesExpected 剩余字节数
// *
// */
//typedef void (^downloadProgressBlock)(NSUInteger bytes, long long totalBytes, long long totalBytesExpected);
//typedef void (^messageFailCallback)(void);
//
//typedef NS_ENUM(NSUInteger,WBNewBaseModelType) {
//    WBNewBaseModelTypeDefault=0,
//    WBNewBaseModelTypeStatusAndHeaders
//};
//
//NS_ASSUME_NONNULL_BEGIN
//
//@interface BaseModel : NSObject{
//    BOOL _showIndicator;//是否显示loading指示器，默认为YES
//    __weak UIView *_indicatorContainer;//loadign指示器父视图,不设置时，默认为当前window
//    NSString *_baseURL;//restful前缀
//    __weak NSURLSessionTask *_currentRequestOperation;//当前请求对象
//
//    MBProgressHUD *HUD;//loading管理
//}
//
//@property (nonatomic,assign) BOOL showIndicator;
//@property (nonatomic,weak) UIView *indicatorContainer;
//@property (nonatomic, copy) messageFailCallback failCallback;
//@property (nonatomic, assign) BOOL isSilence;
//@property (nonatomic, assign) BOOL isApi;
//@property (nonatomic, strong) __block AFHTTPSessionManager *httpRequestManager;//请求队列管理
//@property (nonatomic, assign) BOOL needSignature;
//@property (nonatomic, strong) NSString *currentMethod;
//@property (nonatomic, strong) NSDictionary *currentParamsDic;
//@property (nonatomic, strong) NSString *currentRequsetURL;
//@property (nonatomic, assign) NSInteger statusCode;
//@property (nonatomic, strong) NSString *mimeType;
//@property (nonatomic, assign) BOOL isAuthCode;//xj---验证短信验证码
//@property (nonatomic, assign) BOOL authLogin;//xj---登录界面验证手机号是否注册过
//@property (nonatomic, strong) NSMutableDictionary * requestSettingsDic;
//
///**
// *  构造函数
// *
// *  @param indicator 请求时是否显示指示器
// *
// *  @return 模型实例
// */
//-(instancetype) initWithShowIndicator:(BOOL) indicator;
//
///**
// *  构造函数
// *
// *  @param indicator 请求时是否显示指示器
// *  @param container 指示器父级视图，默认为当前window，不想阻塞界面时，请给此属性赋值
// *
// *  @return 模型实例
// */
//-(instancetype) initWithShowIndicator:(BOOL) indicator container:(UIView*)container;
//
///**
// *  发送get请求到服务器
// *
// *  @param url    请求子URL
// *  @param invoke 请求回调
// */
//-(void) startGet:(NSString*)url callBack:(modelCallBack) invoke;
//
//-(void) startGet:(NSString*)url callBack:(modelCallBack) invoke type:(WBNewBaseModelType)type;
//
//-(void) startGetAddNetwork_id:(NSString*)url network_id:(int)network_id callBack:(modelCallBack) invoke ;
///**
// *  发送post请求到服务器
// *
// *  @param url    请求子URL
// *  @param params 请求参数
// *  @param invoke 请求回调
// */
//-(void) startPost:(NSString*)url params:(NSDictionary*)params callBack:(modelCallBack) invoke;
//
//-(void) startPost:(NSString*)url params:(NSDictionary*)params callBack:(modelCallBack) invoke  type:(WBNewBaseModelType)type;
///**
// *  发送带附件的post请求到服务器
// *
// *  @param url　　　　　请求子URL
// *  @param params     请求参数
// *  @param attachment 请求附件
// *  @param invoke     请求回调
// */
//-(void) startPost:(NSString *)url params:(NSDictionary *)params attachment:(NSDictionary*)attachment callBack:(modelCallBack)invoke;
//
///**
// *  发送put请求到服务器
// *
// *  @param url    请求子URL
// *  @param params 请求参数
// *  @param invoke 请求回调
// */
//-(void) startPut:(NSString*)url params:(NSDictionary*)params callBack:(modelCallBack) invoke;
//-(void) startPut:(NSString*)url params:(NSDictionary*)params callBack:(modelCallBack) invoke type:(WBNewBaseModelType)type;
///**
// *  发送delete请求到服务器
// *
// *  @param url    请求子URL
// *  @param params 请求参数
// *  @param invoke 请求回调
// */
//-(void) startDelete:(NSString*)url params:(NSDictionary*)params callBack:(modelCallBack) invoke;
//
//-(void) startDelete:(NSString*)url params:(NSDictionary*)params callBack:(modelCallBack) invoke type:(WBNewBaseModelType)type;
///**
// *  下载远程文件到指定路径
// *
// *  @param URI               远程文件路径
// *  @param filePath          本地保存文件爱你路径
// *  @param progress          下载进度
// *  @param destination
// *  @param completionHandler
// */
//-(void) downLoadFile:(NSString*)URI
//            savePath:(NSString*)filePath
//            progress:(downloadProgressBlock)progress
//   completionHandler:(void (^)(NSString *filePath, NSError *error))completionHandler;
//
//-(void) downLoadFileWithNoHUD:(NSString*)URI
//                     savePath:(NSString*)filePath
//                     progress:(downloadProgressBlock)progress
//            completionHandler:(void (^)(NSString *filePath, NSError *error))completionHandler;
//
//
///**
// *  取消当前正在发送的请求
// */
//-(void) cancel;
//
///**
// *  取消队列中所有未执行的和正在执行的请求
// */
//-(void) cancelAll;
//
///**
// *  暂停当前正在发送的请求
// */
//- (void)suspend;
//
///**
// *  继续当前正在发送的请求
// */
//- (void)resume;
//
///**
// *  设置HTTP请求
// */
//-(void) setupHttpRqeust;
//
//-(void) addHUD;
//
//-(void) removeHUD;
//
//-(BOOL)isShowingHUD;
//
//-(void)setExtHeaders:(NSDictionary*)params;
//-(void)setTimeoutInterval:(NSTimeInterval)timeoutInterval;
//
////-(void)startAppTunnelRequset:(NSURLRequest *)appTunnerRequset completionHandler:(appTunnelCallback)callback;
//- (void)startAppTunnelRequset:(NSURLRequest *)appTunnerRequset
//               uploadProgress:(void (^)(NSProgress *uploadProgress))uploadProgressBlock
//             downloadProgress:(void (^)(NSProgress *downloadProgress))downloadProgressBlock
//            completionHandler:(appTunnelCallback)completionHandler;
//
//
////可配置请求设置的请求接口
//-(void) startGet:(NSString*)url callBack:(modelCallBack) invoke setting:(BaseRequestSetting *)setting;
//- (void)startPost:(NSString *)url params:(NSDictionary *)params callBack:(modelCallBack)invoke setting:(BaseRequestSetting *)setting;
//-(void) startPost:(NSString *)url params:(NSDictionary *)params attachment:(NSDictionary*)attachment callBack:(modelCallBack)invoke setting:(BaseRequestSetting *)setting;
//-(void) startPut:(NSString*)url params:(NSDictionary*)params callBack:(modelCallBack) invoke setting:(BaseRequestSetting *)setting;
//-(void) startDelete:(NSString*)url params:(NSDictionary*)params callBack:(modelCallBack) invoke setting:(BaseRequestSetting *)setting;
//
//- (void)saveRequestSetting:(BaseRequestSetting *)settingVO Method:(NSString *)method Url:(NSString *)url Params:(NSDictionary *)params;
//
////下载任务
//- (NSURLSessionDownloadTask *)startDownloadTaskWithUrl:(NSString *)url destinationUrl:(NSURL *)destination progress:(progressCallback)downloadProgressBlock completionHandler:(void (^)(NSURLResponse *response, NSURL *filePath, NSError *error))completionHandler;
//
//@end
//
//NS_ASSUME_NONNULL_END
