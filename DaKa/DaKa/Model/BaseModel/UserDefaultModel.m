////
////  UserDefaultModel.m
////  DaKa
////
////  Created by 刁俊方 on 2020/1/5.
////  Copyright © 2020 刁俊方. All rights reserved.
////
//
//#import "UserDefaultModel.h"
//
//@implementation UserDefaultModel {
//    NSMutableDictionary *cacheDic;
//}
//
//@synthesize dbQueue;
//
//+ (UserDefaultModel *)shareInstance {
//    static UserDefaultModel *dbInstance = nil;
//    static dispatch_once_t once;
//    dispatch_once(&once, ^{
//        dbInstance = [[self alloc] init];
//        [dbInstance initCache];
//        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//        NSString *documentDirectory = [paths objectAtIndex:0];
//        NSString *dbPath = [documentDirectory stringByAppendingPathComponent:@"/Documents/UserDefaultEncrpty.sqlite"];
//        NSFileManager *fileManager = [[NSFileManager alloc] init];
//        if(![fileManager fileExistsAtPath:dbPath]) {
//            NSString *docPath = [documentDirectory stringByAppendingString:@"/Documents"];
//            if (![fileManager fileExistsAtPath:docPath]) {
//                [fileManager createDirectoryAtPath:docPath withIntermediateDirectories:YES attributes:nil error:nil];
//            }
//            [fileManager createFileAtPath:dbPath contents:nil attributes:[NSDictionary dictionaryWithObject:NSFileProtectionNone forKey:NSFileProtectionKey]];
//        }
//        dbInstance.dbQueue = [FMDatabaseQueue databaseQueueWithPath:dbPath];
//        [dbInstance createEncrptyDBTable];
//        [dbInstance createBinaryEncrptyDBTable];
//    });
//    return dbInstance;
//}
//
//-(void)initCache {
//    cacheDic = [NSMutableDictionary new];
//}
//
//-(void)createEncrptyDBTable {
//    NSString *sqlCreateTable =  [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS 'UserDefault' ('DBkey' TEXT PRIMARY KEY NOT NULL, 'DBvalue' TEXT)"];
//    [dbQueue inDatabase:^(FMDatabase *db) {
//        [db executeUpdate:sqlCreateTable];
//    }];
//}
//
//-(void)createBinaryEncrptyDBTable {
//    NSString *sqlCreateTable =  [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS 'UserDefaultBinary' ('DBkey' TEXT PRIMARY KEY NOT NULL, 'DBvalue' BLOB)"];
//    [dbQueue inDatabase:^(FMDatabase *db) {
//        [db executeUpdate:sqlCreateTable];
//    }];
//}
//
//-(BOOL)updateDBWithKey:(NSString *)key withValue:(NSString *)value {
//    __block BOOL result = NO;
//    [dbQueue inDatabase:^(FMDatabase *db) {
//        [self->cacheDic setValue:value forKey:key];
//        result = [db executeUpdate:@"Replace Into UserDefault (DBkey, DBvalue) values (?,?)", key, value];
//    }];
//
//    NSLog(@"finish update db========");
//    return result;
//}
//
//-(NSString *)getDBValueWithKey:(NSString *)key {
//
//    NSString *value = [cacheDic objectForKey:key];
//    if(value) {
//        return value;
//    }
//
//    __block NSString *result = nil;
//    [dbQueue inDatabase:^(FMDatabase *db) {
//        //executeUpdate
//        FMResultSet *resultSet = [db executeQuery:@"SELECT * FROM UserDefault WHERE DBkey=? ",key];
//        while ([resultSet next]) {
//            result = [resultSet stringForColumn:@"DBvalue"];
//            if(result) {
//                [self->cacheDic setObject:result forKey:key];
//            }
//            break;
//        }
//        [resultSet close];
//    }];
//
//    return result;
//}
//
//-(BOOL)deleteDBWithKey:(NSString *)key {
//    __block BOOL result = NO;
//    [dbQueue inDatabase:^(FMDatabase *db) {
//        [self->cacheDic removeObjectForKey:key];
//        //executeUpdate
//        result = [db executeUpdate:@"DELETE FROM UserDefault WHERE DBkey=?",key];
//    }];
//    return result;
//}
//
//
//
//-(BOOL)updateDBWithKey:(NSString *)key withBinaryValue:(NSData *)binaryValue {
//    if(!binaryValue || !key) {
//        return NO;
//    }
//    __block BOOL result = NO;
//    [dbQueue inDatabase:^(FMDatabase *db) {
//        [self->cacheDic setValue:binaryValue forKey:key];
//        result = [db executeUpdate:@"Replace Into UserDefaultBinary  (DBkey, DBvalue) values (?,?)", key, binaryValue];
//    }];
//    return result;
//}
//
//-(NSData *)getDBBinaryValueWithKey:(NSString *)key {
//    NSData *value = [cacheDic objectForKey:key];
//    if(value) {
//        return value;
//    }
//    __block NSData *result = nil;
//    [dbQueue inDatabase:^(FMDatabase *db) {
//        FMResultSet *resultSet = [db executeQuery:@"SELECT * FROM UserDefaultBinary WHERE DBkey=? ",key];
//        while ([resultSet next]) {
//            result = [resultSet dataForColumn:@"DBvalue"];
//            if(result) {
//                [self->cacheDic setObject:result forKey:key];
//            }
//            break;
//        }
//        [resultSet close];
//    }];
//
//    return result;
//}
//
//-(BOOL)deleteDBBinaryWithKey:(NSString *)key {
//    __block BOOL result = NO;
//    [dbQueue inDatabase:^(FMDatabase *db) {
//        [self->cacheDic removeObjectForKey:key];
//        result = [db executeUpdate:@"DELETE FROM UserDefaultBinary WHERE DBkey=?",key];
//    }];
//    return result;
//}
//
//@end
