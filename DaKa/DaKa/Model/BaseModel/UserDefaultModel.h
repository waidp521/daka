////
////  UserDefaultModel.h
////  DaKa
////
////  Created by 刁俊方 on 2020/1/5.
////  Copyright © 2020 刁俊方. All rights reserved.
////
//
//#import <Foundation/Foundation.h>
//#import "FMDB.h"
//
//NS_ASSUME_NONNULL_BEGIN
//
//@interface UserDefaultModel : NSObject
//
//@property (nonatomic, strong) FMDatabaseQueue *dbQueue;
//
//
//+(UserDefaultModel *)shareInstance;
//
//-(void)initCache;
//
//-(BOOL)updateDBWithKey:(NSString *)key withValue:(NSString *)value;
//-(NSString *)getDBValueWithKey:(NSString *)key;
//-(BOOL)deleteDBWithKey:(NSString *)key;
//
//-(BOOL)updateDBWithKey:(NSString *)key withBinaryValue:(NSData *)binaryValue;
//-(NSData *)getDBBinaryValueWithKey:(NSString *)key;
//-(BOOL)deleteDBBinaryWithKey:(NSString *)key;
//
//@end
//
//NS_ASSUME_NONNULL_END
