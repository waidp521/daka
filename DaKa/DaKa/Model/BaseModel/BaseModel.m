////
////  BaseModel.m
////  DaKa
////
////  Created by 刁俊方 on 2020/1/5.
////  Copyright © 2020 刁俊方. All rights reserved.
////
//
//#import "BaseModel.h"
////#import "DDLog.h"
//#import "Notification.h"
//#import "NetworkConfig.h"
//#import "FileUtils.h"
//
//static const DDLogLevel ddLogLevel = DDLogLevelDebug;
//
//@implementation BaseModel{
//    UIViewController *viewController;
//    NSTimer *timer;
//}
//
//@synthesize showIndicator = _showIndicator;
//@synthesize indicatorContainer = _indicatorContainer;
//
//#pragma  mark - initialize
//-(instancetype) init {
//    self = [super init];
//    if (self) {
//        _showIndicator = YES;
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cancel) name:KcancelNetwork object:nil];
//
//        [self setupHttpRqeust];
//    }
//    return self;
//}
//
//-(instancetype) initWithShowIndicator:(BOOL) indicator {
//    self = [self init];
//    _showIndicator = indicator;
//    if (!indicator) {
//        _indicatorContainer = nil;
//    }
//    return self;
//}
//
//-(instancetype) initWithShowIndicator:(BOOL) indicator container:(UIView*)container {
//    self = [self init];
//    _showIndicator = indicator;
//    _indicatorContainer = container;
//    if (indicator && !container) {
//        _indicatorContainer = [UIApplication sharedApplication].keyWindow;
//    }
//    return self;
//}
//
//-(void) setupHttpRqeust {
//    _baseURL = BASE_URL;
//    if(!_httpRequestManager){
//        _httpRequestManager = [AFHTTPSessionManager manager];
//    }
//
//    _httpRequestManager.requestSerializer = [AFHTTPRequestSerializer serializer];
//    //北京银行超时时间设置为30秒
//    _httpRequestManager.requestSerializer.timeoutInterval = 30.0f;
//    [_httpRequestManager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
//
//    _httpRequestManager.responseSerializer = [AFJSONResponseSerializer serializer];
//    AFHTTPResponseSerializer *responseSerializer = _httpRequestManager.responseSerializer;
//    NSSet *supportContentTypes = responseSerializer.acceptableContentTypes;
//    NSMutableSet *newSet = [NSMutableSet setWithSet:supportContentTypes];
//    [newSet addObject:@"text/html"];
//    _httpRequestManager.responseSerializer.acceptableContentTypes = newSet;
//}
//
//-(void) addHUD {
//    dispatch_async(dispatch_get_main_queue(), ^{
//        if (self->HUD && [self->HUD superview]) {
//            self->HUD.hidden = NO;
//            return;
//        }
//        if (self->HUD) {
//            [self->HUD removeFromSuperview];
//            self->HUD = nil;
//        }
//        if (self.showIndicator) {
//            UIView *v = self.indicatorContainer;
//            if (!v) {
//                v = [UIApplication sharedApplication].keyWindow;
//            }
//            self->HUD = [[MBProgressHUD alloc] initWithView:v];
//            self->HUD.animationType = MBProgressHUDAnimationZoom;
//
//            [v addSubview:self->HUD];
//
//            [self->HUD showAnimated:YES];
//        }
//    });
//}
//
//-(void) removeHUD {
//    if (HUD) {
//        [HUD removeFromSuperview];
//        HUD = nil;
//    }
//}
//
//#pragma  mark - send request
//-(void) startGet:(NSString*)url callBack:(modelCallBack) invoke type:(WBNewBaseModelType)type{
//    NSString *fullUrl = url;
//    if([url hasPrefix:@"/"]) {
//        fullUrl = [NSString stringWithFormat:@"%@%@",_baseURL,url];
//    }
//    //追加OAuth认证
////    [self appendOAuth:[[NSDictionary alloc] initWithObjectsAndKeys:fullUrl,@"url", nil]];
//    //追加Loading
//    [self addHUD];
//
////    DBLog(@"GET请求 上送URL：%@",fullUrl);
//
//    //为了缓存当前的请求参数，为了需要自签证书的时候重新发起请求
//    _currentMethod = @"GET";
//    _currentRequsetURL = fullUrl;
//    _currentParamsDic = nil;
//
//    BaseRequestSetting * settingVO = [self getRequestSettingWithMethod:_currentMethod Url:url Params:nil];
//
//    if (type==WBNewBaseModelTypeDefault) {
//        _httpRequestManager.responseSerializer = [AFJSONResponseSerializer serializer];
//        AFHTTPResponseSerializer *responseSerializer = _httpRequestManager.responseSerializer;
//        NSSet *supportContentTypes = responseSerializer.acceptableContentTypes;
//        NSMutableSet *newSet = [NSMutableSet setWithSet:supportContentTypes];
//        [newSet addObject:@"text/html"];
//        [newSet addObject:@"text/plain"];
//        [newSet addObject:@"text/javascript"];
//        [newSet addObject:@"text/xml"];
//        [newSet addObject:@"text/html"];
//        [newSet addObject:@"text/json"];
//        [newSet addObject:@"application/json"];
//        _httpRequestManager.responseSerializer.acceptableContentTypes = newSet;
//    }else if(type==WBNewBaseModelTypeStatusAndHeaders){
//        _httpRequestManager.responseSerializer = [AFHTTPResponseSerializer serializer];
//        _httpRequestManager.responseSerializer.acceptableContentTypes= [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", @"text/xml", @"text/plain", nil];
//    }
//    _currentRequestOperation = [_httpRequestManager GET:fullUrl parameters:nil progress:nil success:^(NSURLSessionTask *operation, id responseObject) {
//
//        if (settingVO.mustBeSend) {
//            [WBResendModel deleteRequest:_currentRequestOperation Method:_currentMethod Url:url Params:nil];
//        }
//
//        if (_showIndicator) {
//            [HUD hide:YES];
//        }
//        [self parseResponse:operation responseData:responseObject callBack:invoke type:type withSetting:settingVO];
//    } failure:^(NSURLSessionTask *operation, NSError *error) {
//        if (_showIndicator) {
//            [HUD hide:YES];
//        }
//
//        if (settingVO.reconnectionTimes>0) {
//            settingVO.reconnectionTimes-=1;
//            [self saveRequestSetting:settingVO Method:_currentMethod Url:url Params:nil];
//            [self startGet:url callBack:invoke];
//        }else{
//            [self parseFailure:operation error:error callBack:invoke type:type withSetting:settingVO];
//        }
//
//    }];
//    if (settingVO.mustBeSend) {
//        [WBResendModel saveRequest:_currentRequestOperation Method:_currentMethod Url:url Params:nil];
//    }
//}
//
//- (BaseRequestSetting *)getRequestSettingWithMethod:(NSString *)method Url:(NSString *)url Params:(NSDictionary *)params{
//    NSString * key = [NSString stringWithFormat:@"%@-%@-%@",method,url,params];
//    return [self.requestSettingsDic valueForKey:key];
//}
//
//@end
