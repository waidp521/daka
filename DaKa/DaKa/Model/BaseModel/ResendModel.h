////
////  ResendModel.h
////  DaKa
////
////  Created by 刁俊方 on 2020/1/5.
////  Copyright © 2020 刁俊方. All rights reserved.
////
//
//#import <Foundation/Foundation.h>
//
//NS_ASSUME_NONNULL_BEGIN
//
//@interface ResendModel : NSObject
//
//+ (void)saveRequest:(NSURLSessionTask *)sessionTask Method:(NSString *)method Url:(NSString *)url Params:(NSDictionary *)params;
//
//+ (void)deleteRequest:(NSURLSessionTask *)sessionTask Method:(NSString *)method Url:(NSString *)url Params:(NSDictionary *)params;
//
//+ (void)resendAllSavedRequest;
//
//@end
//
//NS_ASSUME_NONNULL_END
