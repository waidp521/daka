//
//  FileUtils.h
//  DaKa
//
//  Created by 刁俊方 on 2020/1/5.
//  Copyright © 2020 刁俊方. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FileUtils : NSObject

+(FileUtils*) sharedFileUtils;

-(NSString *)getBaseURL;

@end

NS_ASSUME_NONNULL_END
