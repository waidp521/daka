//
//  SysUtils.m
//  DaKa
//
//  Created by 刁俊方 on 2020/1/5.
//  Copyright © 2020 刁俊方. All rights reserved.
//

#import "SysUtils.h"

static SysUtils *sysUtils;

@implementation SysUtils

+(SysUtils*) sharedSysUtils {
    if (sysUtils == nil) {
        @synchronized(self) {
            if (sysUtils == nil) {
                sysUtils = [[SysUtils alloc] init];
            }
        }
    }
    return sysUtils;
}

- (CGFloat)heightOfString:(NSString *)string font:(UIFont *)font width:(CGFloat)width
{
    if (string == nil) {
        return 0;
    }
    
    CGSize size=CGSizeZero;
    if ([[UIDevice currentDevice].systemVersion doubleValue]<7.0) {
        size=[string sizeWithFont:font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    }else{
        size = [string boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX)
                                    options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingTruncatesLastVisibleLine
                                 attributes:@{NSFontAttributeName:font}
                                    context:nil].size;
    }
    return ceilf(size.height);
}
- (CGFloat)widthOfString:(NSString *)string font:(UIFont *)font height:(CGFloat)height {
    if (string == nil) {
        return 0;
    }
    
    CGSize size=CGSizeZero;
    if ([[UIDevice currentDevice].systemVersion doubleValue]<7.0) {
        size=[string sizeWithFont:font constrainedToSize:CGSizeMake(CGFLOAT_MAX, height)];
    }else{
        size = [string boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, height)
                                    options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingTruncatesLastVisibleLine
                                 attributes:@{NSFontAttributeName:font}
                                    context:nil].size;
    }
    return ceilf(size.width);
}

@end
