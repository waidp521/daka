//
//  SysUtils.h
//  DaKa
//
//  Created by 刁俊方 on 2020/1/5.
//  Copyright © 2020 刁俊方. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SysUtils : NSObject

+(SysUtils*) sharedSysUtils;

- (CGFloat)heightOfString:(NSString *)string font:(UIFont *)font width:(CGFloat)width;
- (CGFloat)widthOfString:(NSString *)string font:(UIFont *)font height:(CGFloat)height;

@end

NS_ASSUME_NONNULL_END
