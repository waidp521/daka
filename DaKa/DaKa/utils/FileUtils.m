//
//  FileUtils.m
//  DaKa
//
//  Created by 刁俊方 on 2020/1/5.
//  Copyright © 2020 刁俊方. All rights reserved.
//

#import "FileUtils.h"
#import "UserDefaultModel.h"

static FileUtils *fileUtils = nil;

@implementation FileUtils

+ (FileUtils *)sharedFileUtils {
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        fileUtils = [[self alloc] init];
    });
    return fileUtils;
}

- (NSString *)getBaseURL {
    NSString *baseURL = nil;
    NSString *port = [fileUtils getUserDefaultsForKey:@"minxingPort"];
    if(port && port.length > 0 && port.intValue != 80 && port.intValue != 443)
    {
        baseURL = [NSString stringWithFormat:@"%@:%@", [fileUtils getUserDefaultsForKey:@"minxingURL"], [fileUtils getUserDefaultsForKey:@"minxingPort"]];
    }
    else
    {
        baseURL = [fileUtils getUserDefaultsForKey:@"minxingURL"];
    }
    return  baseURL;
}

-(id)getUserDefaultsForKey:(NSString*)key {
//    UserDefaultModel *userDefaultModel = [UserDefaultModel shareInstance];
//    return [userDefaultModel getDBValueWithKey:key];
    return @"";
}

@end
