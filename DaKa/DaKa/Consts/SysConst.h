//
//  SysConst.h
//  DaKa
//
//  Created by 刁俊方 on 2020/1/5.
//  Copyright © 2020 刁俊方. All rights reserved.
//

#ifndef SysConst_h
#define SysConst_h

#define GetLocalResStr(key) NSLocalizedStringFromTable(key, @"DKStrings", key)

// 标准系统状态栏高度
#define SYS_STATUSBAR_HEIGHT 20
// 热点栏高度
#define HOTSPOT_STATUSBAR_HEIGHT 20
// APP_STATUSBAR_HEIGHT=SYS_STATUSBAR_HEIGHT+[HOTSPOT_STATUSBAR_HEIGHT]
#define APP_STATUSBAR_HEIGHT  (CGRectGetHeight([UIApplication sharedApplication].statusBarFrame))
// 根据APP_STATUSBAR_HEIGHT判断是否存在热点栏
#define IS_HOTSPOT_CONNECTED  (APP_STATUSBAR_HEIGHT==(SYS_STATUSBAR_HEIGHT+HOTSPOT_STATUSBAR_HEIGHT)?YES:NO)

#define appWidth  [UIScreen mainScreen].bounds.size.width
#define appHeight  (IS_HOTSPOT_CONNECTED?([UIScreen mainScreen].bounds.size.height-HOTSPOT_STATUSBAR_HEIGHT):[UIScreen mainScreen].bounds.size.height)
#define appFrame    (IS_HOTSPOT_CONNECTED?CGRectMake(0, 0,[UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height-HOTSPOT_STATUSBAR_HEIGHT):[UIScreen mainScreen].bounds)

//iphone设备
#define isIphone ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone)
//ipad设备
#define isIpad ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
//iPhoneX设备
#define SCREEN_HEIGHTL [UIScreen mainScreen].bounds.size.height
#define SCREEN_WIDTHL [UIScreen mainScreen].bounds.size.width
#define isIphoneX (((int)((SCREEN_HEIGHTL/SCREEN_WIDTHL)*100) == 216)||((int)((SCREEN_WIDTHL/SCREEN_HEIGHTL)*100)) == 216?YES:NO)

#define mainThemeColor @"#d81e06"

#define mainBackgroundGrayColor @"#8a8a8a"
#define mainTextGrayColor @"bdbdbd"
#define mainBlackColor @"#333333"

#endif /* SysConst_h */
